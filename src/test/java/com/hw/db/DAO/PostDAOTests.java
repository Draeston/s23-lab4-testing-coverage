package com.hw.db.DAO;

import static org.mockito.Mockito.times;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import com.hw.db.models.Post;

public class PostDAOTests {
  JdbcTemplate jdbcTemplate;
  PostDAO postDAO;
  Post post;
  static final Integer postId = 1;
  static final String postAuthor = "author";
  static final Timestamp postCreated = new Timestamp(1);
  static final String postMessage = "message";
  static final String postQuery = "SELECT * FROM \"posts\" WHERE id=? LIMIT 1;";

  private static List<Arguments> argsList() {
    return List.of(
      Arguments.of(
        new Post(
          postAuthor,
          new Timestamp(10),
          "",
          postMessage,
          null,
          null,
          false
        ),
        "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
      ),
      Arguments.of(
        new Post(
          postAuthor,
          new Timestamp(10),
          "",
          "newMessage",
          null,
          null,
          false
        ),
        "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
      ),
      Arguments.of(
        new Post(
          "newAuthor",
          new Timestamp(10),
          "",
          "newMessage",
          null,
          null,
          false
        ),
        "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"
      ),
      Arguments.of(
        new Post(
          "newAuthor",
          postCreated,
          "",
          postMessage,
          null,
          null,
          false
        ),
        "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"
      )
    );
  }

  @BeforeEach
  void init() {
    jdbcTemplate = Mockito.mock(JdbcTemplate.class);
    postDAO = new PostDAO(jdbcTemplate);
    post = new Post(
      postAuthor,
      postCreated,
      "",
      postMessage,
      null,
      null,
      false
    );
    Mockito
      .when(jdbcTemplate.queryForObject(
        Mockito.eq(postQuery),
        Mockito.any(PostDAO.PostMapper.class),
        Mockito.eq(postId)
      ))
      .thenReturn(post);
  }

  @ParameterizedTest
  @MethodSource("argsList")
  void testSetPost(Post newPost, String result) {
    PostDAO.setPost(postId, newPost);
    Mockito
      .verify(jdbcTemplate)
      .update(
        Mockito.eq(result),
        Optional.ofNullable(Mockito.any())
      );
  }

  @Test
  void testSetPostNull() {
    PostDAO.setPost(postId, new Post());
    Mockito
      .verify(jdbcTemplate, times(0))
      .update(
        Mockito.any(), 
        Optional.ofNullable(Mockito.any())
      );
  }
}
