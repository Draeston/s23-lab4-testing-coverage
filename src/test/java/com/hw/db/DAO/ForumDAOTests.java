package com.hw.db.DAO;

import java.util.List;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.junit.jupiter.params.ParameterizedTest;

public class ForumDAOTests {
  JdbcTemplate jdbcTemplate;
  ForumDAO forumDAO;

  private static List<Arguments> argsList() {
    return List.of(
      Arguments.of(
        "",
        100,
        "",
        true,
        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"
      ),
      Arguments.of(
        "",
        null,
        "",
        false,
        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"
      ),
      Arguments.of(
        "",
        null,
        null,
        false,
        "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"
      )
    );
  }

  @BeforeEach
  void init() {
    jdbcTemplate = Mockito.mock(JdbcTemplate.class);
    forumDAO = new ForumDAO(jdbcTemplate);
  }

  @ParameterizedTest
  @MethodSource("argsList")
  void testUserList(
    String slug, 
    Number limit, 
    String since, 
    Boolean desc,
    String result
  ) {
    ForumDAO.UserList(slug, limit, since, desc);
    Mockito
      .verify(jdbcTemplate)
      .query(
        Mockito.eq(result),
        Mockito.any(Object[].class),
        Mockito.any(UserDAO.UserMapper.class)
      );
  }
}