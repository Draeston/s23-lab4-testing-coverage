package com.hw.db.DAO;

import static org.mockito.Mockito.verifyNoInteractions;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import com.hw.db.models.User;

public class UserDAOTests {
  JdbcTemplate jdbcTemplate;
  UserDAO userDAO;

  private static List<Arguments> argsList() {
    return List.of(
      Arguments.of(
        new User(
          null, 
          "", 
          "", 
          ""),
        "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"
      ),
      Arguments.of(
        new User(
          null, 
          null, 
          "", 
          ""),
        "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"
      ),
      Arguments.of(
        new User(
          null, 
          "", 
          null, 
          ""),
        "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"
      ),
      Arguments.of(
        new User(
          null, 
          "", 
          "", 
          null),
        "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"
      ),
      Arguments.of(
        new User(
          null, 
          null, 
          null, 
          ""),
        "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"
      ),
      Arguments.of(
        new User(
          null, 
          null, 
          "", 
          null),
        "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"
      ),
      Arguments.of(
        new User(
          null, 
          "", 
          null, 
          null),
        "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"
      )
    );
  }

  @BeforeEach
  void init() {
    jdbcTemplate = Mockito.mock(JdbcTemplate.class);
    userDAO = new UserDAO(jdbcTemplate);
  }

  @ParameterizedTest
  @MethodSource("argsList")
  void testChange(User user, String result) {
    UserDAO.Change(user);
    Mockito
      .verify(jdbcTemplate)
      .update(
        Mockito.eq(result),
        Optional.ofNullable(Mockito.any())
      );
  }

  @Test
  void testChangeNull() {
    UserDAO.Change(new User(null, null, null, null));
    verifyNoInteractions(jdbcTemplate);
  }
}
