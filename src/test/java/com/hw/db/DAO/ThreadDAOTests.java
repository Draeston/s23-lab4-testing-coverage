package com.hw.db.DAO;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ThreadDAOTests {
  JdbcTemplate jdbcTemplate;
  ThreadDAO threadDAO;

  private static List<Arguments> argsList() {
    return List.of(
      Arguments.of(
        1,
        1,
        1,
        true,
        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"
      ),
      Arguments.of(
        1,
        1,
        1,
        false,
        "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"
      )
    );
  }

  @BeforeEach
  void init() {
    jdbcTemplate = Mockito.mock(JdbcTemplate.class);
    threadDAO = new ThreadDAO(jdbcTemplate);
  }

  @ParameterizedTest
  @MethodSource("argsList")
  void testTreeSort(Integer id, Integer limit, Integer since, Boolean desc, String result) {
    ThreadDAO.treeSort(id, limit, since, desc);
    Mockito
      .verify(jdbcTemplate)
      .query(
        Mockito.eq(result),
        Mockito.any(PostDAO.PostMapper.class),
        Optional.ofNullable(Mockito.any())
      );
  }
}
